package routers

import (
	"vishnu/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.CreateAppListController{})
    beego.Router("/blank", &controllers.BlankController{})
    beego.Router("/env", &controllers.EnvController{})
    
    beego.Router("/createapplist", &controllers.CreateAppListController{})
    beego.Router("/createappform", &controllers.CreateAppFormController{})
    beego.Router("/createapp", &controllers.CreateAppController{})
    beego.Router("/viewapp", &controllers.ViewAppController{})
	beego.Router("/deleteapp", &controllers.DeleteAppController{})

    beego.Router("/viewcluster", &controllers.ViewClusterController{})
    
    beego.Router("/dockerregistry", &controllers.DockerRegisteryController{})
    beego.Router("/dockerrepository", &controllers.DockerRepositoryController{})
    beego.Router("/applications", &controllers.ViewApplicationsController{})
    
    beego.Router("/settings", &controllers.SettingsController{})
    beego.Router("/updatedrsettings", &controllers.SettingsController{},"get:UpdateDR")
    beego.Router("/updateghsettings", &controllers.SettingsController{},"get:UpdateGH")
    
    beego.Router("/old", &controllers.MainController{})
    beego.Router("/create/meanapp", &controllers.CreateMeanAppController{})
    
    beego.Router("/create/nodeapp", &controllers.CreateNodeAppController{})
    beego.Router("/action/createnodeapp", &controllers.ActionCreateNodeAppController{})
    beego.Router("/view/nodeapp", &controllers.ViewNodeAppController{})
    beego.Router("/action/deletenodeapp", &controllers.ActionDeleteNodeAppController{})
    
    beego.Router("/view/nginxapp", &controllers.ViewNginxAppController{})
    beego.Router("/create/nginxapp", &controllers.CreateNginxAppController{})
    beego.Router("/delete/nginxapp", &controllers.DeleteNginxAppController{})
    
    beego.Router("/create/mongodb", &controllers.CreateMongoDbController{})

    
    beego.Router("/dockerrepo", &controllers.DockerRepoAppController{})
    beego.Router("/githubrepo", &controllers.GithubRepoAppController{})
    beego.Router("/", &controllers.AppController{})
    beego.Router("/get/componentstatuses", &controllers.ComponentstatusesController{})
    beego.Router("/get/configmaps", &controllers.ConfigmapsController{})
    beego.Router("/get/daemonsets", &controllers.DaemonsetsController{})
    beego.Router("/get/deployments", &controllers.DeploymentsController{})
    beego.Router("/get/events", &controllers.EventsController{})
    beego.Router("/get/endpoints", &controllers.CndpointsController{})
    beego.Router("/get/horizontalpodautoscalers", &controllers.HorizontalpodautoscalersController{})
    beego.Router("/get/ingress", &controllers.IngressController{})
    beego.Router("/get/jobs", &controllers.JobsController{})
    beego.Router("/get/limitranges", &controllers.LimitrangesController{})
    beego.Router("/get/nodes", &controllers.NodesController{})
    beego.Router("/get/namespaces", &controllers.NamespacesController{})
    beego.Router("/get/pods", &controllers.PodsController{})
    beego.Router("/get/persistentvolumes", &controllers.PersistentvolumesController{})
    beego.Router("/get/persistentvolumeclaims", &controllers.PersistentvolumeclaimsController{})
    beego.Router("/get/quota", &controllers.QuotaController{})
    beego.Router("/get/resourcequotas", &controllers.ResourcequotasController{})
    beego.Router("/get/replicasets", &controllers.ReplicasetsController{})
    beego.Router("/get/replicationcontrollers", &controllers.ReplicationcontrollersController{})
    beego.Router("/get/secrets", &controllers.SecretsController{})
    beego.Router("/get/serviceaccounts", &controllers.ServiceaccountsController{})
    beego.Router("/get/services", &controllers.ServicesController{})
}
