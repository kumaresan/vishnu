package controllers

import (
	"github.com/astaxie/beego"
	"flag"
	"fmt"
	"log"
	"os"
//    "net/url"
	"k8s.io/client-go/1.4/kubernetes"
	"k8s.io/client-go/1.4/pkg/api"
	"k8s.io/client-go/1.4/tools/clientcmd"	
	"k8s.io/client-go/1.4/pkg/api/errors"
//	"k8s.io/client-go/1.4/pkg/api/resource"
	"k8s.io/client-go/1.4/pkg/api/unversioned"
	"k8s.io/client-go/1.4/pkg/api/v1"
	"k8s.io/client-go/1.4/pkg/apis/extensions/v1beta1"
	"k8s.io/client-go/1.4/pkg/util/intstr"
//----------------------------------------------------	
    "github.com/heroku/docker-registry-client/registry"
//----------------------------------------------------    
//    "github.com/google/go-github/github"
//    "golang.org/x/oauth2"

) 

const namespace string = "kman" 




type AppController struct {
	beego.Controller
}

func (c *AppController) Get() {
	c.TplName = "layout_app.tpl"
}


//---------------------------------------------------------------
type CreateNodeAppController struct {
	beego.Controller
}

func (c *CreateNodeAppController) Get() {
	fmt.Printf("CreateNodeAppController Get\n")
	c.TplName = "create_node_app.tpl"
}

type ActionCreateNodeAppController struct {
	beego.Controller
}

func (c *ActionCreateNodeAppController) Get() {
	fmt.Printf("ActionCreateNodeAppController Get\n")
	c.Redirect("/view/nodeapp", 302)
}

type ViewNodeAppController struct {
	beego.Controller
}

func (c *ViewNodeAppController) Get() {
	fmt.Printf("ViewNodeAppController Get\n")
	
	c.TplName = "view_node_app.tpl"
}

type ActionDeleteNodeAppController struct {
	beego.Controller
}

func (c *ActionDeleteNodeAppController) Get() {
	fmt.Printf("ActionDeleteNodeAppController Get\n")
	c.Redirect("/view/nodeapp", 302)
}




//----------------------------------------------------------------------------

type CreateNginxAppController struct {
	beego.Controller
}

type operation interface {
	Do(c *kubernetes.Clientset)
}

type deployOperation struct {
	image string
	name  string
	port  int
}

func (op *deployOperation) Do(c *kubernetes.Clientset) {
	if err := op.doDeployment(c); err != nil {
		log.Fatal(err)
	}

	if err := op.doService(c); err != nil {
		log.Fatal(err)
	}
}

func (op *deployOperation) doDeployment(c *kubernetes.Clientset) error {
	appName := op.name

	// Define Deployments spec.
	deploySpec := &v1beta1.Deployment{
		TypeMeta: unversioned.TypeMeta{
			Kind:       "Deployment",
			APIVersion: "extensions/v1beta1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name: appName,
		},
		Spec: v1beta1.DeploymentSpec{
			Replicas: int32p(3),
//			Strategy: v1beta1.DeploymentStrategy{
//				Type: v1beta1.RollingUpdateDeploymentStrategyType,
//				RollingUpdate: &v1beta1.RollingUpdateDeployment{
//					MaxUnavailable: &intstr.IntOrString{
//						Type:   intstr.Int,
//						IntVal: int32(0),
//					},
//					MaxSurge: &intstr.IntOrString{
//						Type:   intstr.Int,
//						IntVal: int32(1),
//					},
//				},
//			},
			RevisionHistoryLimit: int32p(10),
			Template: v1.PodTemplateSpec{
				ObjectMeta: v1.ObjectMeta{
					Name:   appName,
					Labels: map[string]string{"app": appName},
				},
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						v1.Container{
							Name:  op.name,
							Image: op.image,
							Ports: []v1.ContainerPort{
								v1.ContainerPort{ContainerPort: int32(op.port), Protocol: v1.ProtocolTCP},
							},
//							Resources: v1.ResourceRequirements{
//								Limits: v1.ResourceList{
//									v1.ResourceCPU:    resource.MustParse("100m"),
//									v1.ResourceMemory: resource.MustParse("256Mi"),
//								},
//							},
							ImagePullPolicy: v1.PullIfNotPresent,
						},
					},
					RestartPolicy: v1.RestartPolicyAlways,
					DNSPolicy:     v1.DNSClusterFirst,
				},
			},
		},
	}

	// Implement deployment update-or-create semantics.
	deploy := c.Extensions().Deployments(namespace)
	_, err := deploy.Update(deploySpec)
	switch {
	case err == nil:
		fmt.Println("deployment controller updated")
	case !errors.IsNotFound(err):
		return fmt.Errorf("could not update deployment controller: %s", err)
	default:
		_, err = deploy.Create(deploySpec)
		if err != nil {
			return fmt.Errorf("could not create deployment controller: %s", err)
		}
		fmt.Println("deployment controller created")
	}

	return nil
}

func (op *deployOperation) doService(c *kubernetes.Clientset) error {
	appName := op.name

	// Define service spec.
	serviceSpec := &v1.Service{
		TypeMeta: unversioned.TypeMeta{
			Kind:       "Service",
			APIVersion: "v1",
		},
		ObjectMeta: v1.ObjectMeta{
			Name: appName,
		},
		Spec: v1.ServiceSpec{
			Type:     v1.ServiceTypeLoadBalancer,
			Selector: map[string]string{"app": appName},
			Ports: []v1.ServicePort{
				v1.ServicePort{
					Protocol: v1.ProtocolTCP,
					Port:     80,					
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: int32(op.port),
					},
				},
			},
		},
	}

	// Implement service update-or-create semantics.
	service := c.Core().Services(namespace)
	svc, err := service.Get(appName)
	switch {
	case err == nil:
		serviceSpec.ObjectMeta.ResourceVersion = svc.ObjectMeta.ResourceVersion
		serviceSpec.Spec.ClusterIP = svc.Spec.ClusterIP
		_, err = service.Update(serviceSpec)
		if err != nil {
			return fmt.Errorf("failed to update service: %s", err)
		}
		fmt.Println("service updated")
	case errors.IsNotFound(err):
		_, err = service.Create(serviceSpec)
		if err != nil {
			return fmt.Errorf("failed to create service: %s", err)
		}
		fmt.Println("service created")
	default:
		return fmt.Errorf("unexpected error: %s", err)
	}

	return nil
}

func int32p(i int32) *int32 {
	r := new(int32)
	*r = i
	return r
}

func (c *CreateNginxAppController) Get() {
	fmt.Printf("CreateNginxAppController Get\n")

	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)

	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	var op operation
	op = &deployOperation{
			image: "nginx",
			name:  "test-nginx",
			port:  80,
		}

	op.Do(clientset)
	
	
	c.Redirect("/view/nginxapp", 302)
}

type DeleteNginxAppController struct {
	beego.Controller
}


func (c *DeleteNginxAppController) Get() {
	fmt.Printf("DeleteNginxAppController Get\n")
	
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)

	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	
	clientset.Deployments(namespace).Delete("test-nginx",getDeleteOptions())
	clientset.Services(namespace).Delete("test-nginx",getDeleteOptions())
	
	c.Redirect("/view/nginxapp", 302)
}

type ViewNginxAppController struct {
	beego.Controller
}


func (c *ViewNginxAppController) Get() {
	
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	config.Host = os.Getenv("KUBERNETES_URL")
	config.Insecure = true
	config.BearerToken = os.Getenv("KUBERNETES_BEARER_TOKEN")

	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	
	pods, err := clientset.Core().Pods("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	}
	
	c.Data["podlist"] = pods.Items
	c.Data["Response"] = pods

	c.TplName = "view_nginx_app.tpl"
}


type CreateMongoDbController struct {
	beego.Controller
}

func (c *CreateMongoDbController) Get() {
	c.TplName = "create_mongo_db.tpl"
}


//----------------------------------------------------------------------------
type DockerRepoAppController struct {
	beego.Controller
}

func (c *DockerRepoAppController) Get() {
	
	url      := os.Getenv("DOCKER_REGISTRY_URL")
	username := os.Getenv("DOCKER_REGISTRY_USERNAME") 
	password := os.Getenv("DOCKER_REGISTRY_PASSWORD") 
	hub, err := registry.New(url, username, password)

	repositories, err := hub.Repositories()
	if err != nil {
		panic(err.Error())
	}

	c.Data["Response"] = repositories
		
	c.TplName = "get_docker_repos.tpl"
}


//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
type CreateMeanAppController struct {
	beego.Controller
}

func (c *CreateMeanAppController) Get() {
	c.TplName = "create_mean_app.tpl"
}

