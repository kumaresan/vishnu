package controllers

import (
	"github.com/astaxie/beego"
	"fmt"
//	"strings"
	"os"
    "net/url"
	"k8s.io/client-go/1.4/kubernetes"
	"k8s.io/client-go/1.4/pkg/api"
	"k8s.io/client-go/1.4/rest"
	"k8s.io/client-go/1.4/pkg/labels"
    "github.com/heroku/docker-registry-client/registry"
    "github.com/google/go-github/github"
    "golang.org/x/oauth2"
) 

var (
	ENV_KUBERNETES_IN_CLUSTER = os.Getenv("KUBERNETES_IN_CLUSTER")
	ENV_KUBERNETES_URL = os.Getenv("KUBERNETES_URL")
	ENV_KUBERNETES_NAMESPACE = os.Getenv("KUBERNETES_NAMESPACE")
	ENV_KUBERNETES_BEARER_TOKEN = os.Getenv("KUBERNETES_BEARER_TOKEN")

//	ENV_DOCKER_REGISTRY_URL = os.Getenv("DOCKER_REGISTRY_URL")
//	ENV_DOCKER_REGISTRY_USERNAME = os.Getenv("DOCKER_REGISTRY_USERNAME")
//	ENV_DOCKER_REGISTRY_PASSWORD = os.Getenv("DOCKER_REGISTRY_PASSWORD")
//
//	ENV_GITHUB_URL = os.Getenv("GITHUB_URL")
//	ENV_GITHUB_ACCESS_TOKEN = os.Getenv("GITHUB_ACCESS_TOKEN")
	
	k8sclientset *kubernetes.Clientset
	drclient registry.Registry
	gitclient github.Client
)

func init() {
	fmt.Printf("Starting Vishnu ============================================\n")
	
	//=========================================================================
	// Initilize environment variables
	
	fmt.Printf("KUBERNETES_IN_CLUSTER     : %s\n", ENV_KUBERNETES_IN_CLUSTER)
	fmt.Printf("KUBERNETES_URL            : %s\n", ENV_KUBERNETES_URL)
	fmt.Printf("KUBERNETES_NAMESPACE      : %s\n", ENV_KUBERNETES_NAMESPACE)
	fmt.Printf("KUBERNETES_BEARER_TOKEN   : %s\n", ENV_KUBERNETES_BEARER_TOKEN)
//	fmt.Printf("DOCKER_REGISTRY_URL       : %s\n", ENV_DOCKER_REGISTRY_URL)
//	fmt.Printf("DOCKER_REGISTRY_USERNAME  : %s\n", ENV_DOCKER_REGISTRY_USERNAME)
//	fmt.Printf("DOCKER_REGISTRY_PASSWORD  : %s\n", ENV_DOCKER_REGISTRY_PASSWORD)
//	fmt.Printf("GITHUB_URL                : %s\n", ENV_GITHUB_URL)
//	fmt.Printf("GITHUB_ACCESS_TOKEN       : %s\n", ENV_GITHUB_ACCESS_TOKEN)

	//=========================================================================
	// Initilize the kubernetes client set.	
	
	fmt.Printf("Initilizing Kubernetes Client\n")
	
	config := &rest.Config{
	  Host:     ENV_KUBERNETES_URL,
	  BearerToken: ENV_KUBERNETES_BEARER_TOKEN,
	  Insecure: true,
	}
	
	k8sclientset,err := kubernetes.NewForConfig(config)

	if err != nil {
		fmt.Printf("Couldn't initilise Kubernetes client\n",err)
		panic(err.Error())
		
	} 
	
	fmt.Printf("Kubernetes Client Initilized\n", k8sclientset)
	fmt.Printf("\n")
		
	fmt.Printf("Initilized Vishnu ============================================\n")
}

//=========================================================================
// Kuberneter Client

func getKubernetesClient() *kubernetes.Clientset {
	
	config, err := rest.InClusterConfig()
	if err != nil {
		config = &rest.Config{
		  Host:     ENV_KUBERNETES_URL,
		  BearerToken: ENV_KUBERNETES_BEARER_TOKEN,
		  Insecure: true,
		}
	}
		
	k8sclientset,err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Printf("Couldn't initilise Kubernetes client\n",err)
		panic(err.Error())
		
	} 
	
	return k8sclientset
}


type BlankController struct {
	beego.Controller
}

func (c *BlankController) Get() {
	c.TplName = "blank.tpl"
}

type EnvController struct {
	beego.Controller
}

func (c *EnvController) Get() {	
	fmt.Printf("EnvController\n")
	c.Data["KUBERNETES_IN_CLUSTER"] = ENV_KUBERNETES_IN_CLUSTER
	c.Data["KUBERNETES_URL"] = ENV_KUBERNETES_URL
	c.Data["KUBERNETES_NAMESPACE"] = ENV_KUBERNETES_NAMESPACE
	c.Data["KUBERNETES_BEARER_TOKEN"] = ENV_KUBERNETES_BEARER_TOKEN
//	c.Data["DOCKER_REGISTRY_URL"] = ENV_DOCKER_REGISTRY_URL
//	c.Data["DOCKER_REGISTRY_USERNAME"] = ENV_DOCKER_REGISTRY_USERNAME
//	c.Data["DOCKER_REGISTRY_PASSWORD"] = ENV_DOCKER_REGISTRY_PASSWORD
//	c.Data["GITHUB_URL"] = ENV_GITHUB_URL
//	c.Data["GITHUB_ACCESS_TOKEN"] = ENV_GITHUB_ACCESS_TOKEN	
	
	c.TplName = "env.tpl"
}

type CreateAppListController struct {
	beego.Controller
}

func (c *CreateAppListController) Get() {
	c.TplName = "create_app_list.tpl"
}


type CreateAppFormController struct {
	beego.Controller
}

func (c *CreateAppFormController) Get() {	
	fmt.Printf("CreateAppFormController , %s , %s \n", c.GetString("apptype"), c.GetString("dockerimage"))
	c.Data["apptype"] = c.GetString("apptype")
	c.Data["dockerimage"] = c.GetString("dockerimage")
	c.TplName = "create_app_form.tpl"
}


type CreateAppController struct {
	beego.Controller
}




func (c *CreateAppController) Get() {	
	fmt.Printf("CreateAppController %s, %s, %s, %s  \n", c.GetString("apptype"),c.GetString("appname"), c.GetString("dockerimage"), c.GetString("appport"))
	
	appname := c.GetString("appname")
	image := c.GetString("dockerimage")
	port,err := c.GetInt("appport")
	if err != nil {
		panic(err.Error())
	}
		
	kubernetesClient := getKubernetesClient()
	
	var op operation
	op = &deployOperation{
			image: image,
			name:  appname,
			port:  port,
		}

	op.Do(kubernetesClient)
	
	c.Redirect("/viewapp?appname=" + appname, 302)
}


type ViewAppController struct {
	beego.Controller
}


func (c *ViewAppController) Get() {	

	appname := c.GetString("appname")
	fmt.Printf("ViewAppController , %s  \n", appname)
	fmt.Printf("ViewAppController , %s  \n", labels.NewSelector().String())
	
	selector,_ := labels.Parse("app=" + appname) 
	
	listOptions := api.ListOptions {
		LabelSelector: selector,
	}
	
	kubernetesClient := getKubernetesClient()
	
	pods, err := kubernetesClient.Core().Pods(ENV_KUBERNETES_NAMESPACE).List(listOptions)
	if err != nil {
		panic(err.Error())
	}

	service, err := getKubernetesClient().Core().Services(ENV_KUBERNETES_NAMESPACE).Get(appname)
	if err != nil {
		panic(err.Error())
	} 
	
	c.Data["service"] = service 
	
	if(service.Status.LoadBalancer.Size() > 0){	
		c.Data["serviceurl"] = "http://" + service.Status.LoadBalancer.Ingress[0].IP + ":80"
	}  else {
		c.Data["serviceurl"] = "Processing.. Referesh Page"
	}
	
	c.Data["appname"] = c.GetString("appname")
	c.Data["podlist"] = pods.Items
	c.Data["Response"] = pods

	c.TplName = "view_app.tpl"
}


type SettingsController struct {
	beego.Controller
}


func (c *SettingsController) UpdateDR() {
	fmt.Println("SettingsController.UpdateDR ")
	drurl := c.GetString("drurl")
	drusername := c.GetString("drusername")
	drpassword := c.GetString("drpassword")	
	
	kubernetesClient := getKubernetesClient()
	dockerRegisterySecret,err :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("docker-registery-secret")

	if err	!= nil {
		dockerRegisterySecret.Name = "docker-registery-secret"		

		data := make(map[string][]byte)
		data["url"] = []byte(drurl)		
		data["username"] = []byte(drusername)
		data["password"] = []byte(drpassword) 
		
		dockerRegisterySecret.Data = data	

		kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Create(dockerRegisterySecret)
	} else {
		
		data := make(map[string][]byte)
		data["url"] = []byte(drurl)		
		data["username"] = []byte(drusername)
		data["password"] = []byte(drpassword) 
				
		dockerRegisterySecret.Data = data
		kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Update(dockerRegisterySecret)		
	}


	c.Redirect("/settings", 302)	
}


func (c *SettingsController) UpdateGH() {	
	fmt.Println("SettingsController.UpdateGH")
	ghurl := c.GetString("ghurl")
	ghtoken := c.GetString("ghtoken")	
		
	
	kubernetesClient := getKubernetesClient()
	gitHubSecret,err :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("github-secret")
	if err	!= nil {
		//panic(err2.Error()) 
		//dockerRegisterySecret := kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Create(*v1.Secret)
	} else {
		gitHubSecret.Data["url"] = []byte(ghurl)
		gitHubSecret.Data["token"] = []byte(ghtoken)	
	}
	kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Update(gitHubSecret)

	c.Redirect("/settings", 302)	
}


func (c *SettingsController) Get() {	

	fmt.Printf("SettingsController , %s  \n")
		
	kubernetesClient := getKubernetesClient()
	
	dockerRegisterySecret,err1 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("docker-registery-secret")
	if err1 != nil {
		//panic(err1.Error())
	}else{
		c.Data["docker_registery_url"] = string(dockerRegisterySecret.Data["url"])
		c.Data["docker_registery_username"] = string(dockerRegisterySecret.Data["username"])
		c.Data["docker_registery_password"] = string(dockerRegisterySecret.Data["password"])	
	}

	gitHubSecret,err2 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("github-secret")
	if err2 != nil {
		//panic(err2.Error())
	}else{
		c.Data["github_url"] = string(gitHubSecret.Data["url"])
		c.Data["github_token"] = string(gitHubSecret.Data["token"])
	}
	

	testSecret,err3 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("test-secret")
	if err3 != nil {
		//panic(err3.Error())
	}else{
		c.Data["test_username"] = string(testSecret.Data["username"]) 
		c.Data["test_password"] = string(testSecret.Data["password"]) 		
	}	
	
	c.Data["docker_registery_secret"] = dockerRegisterySecret
	c.Data["github_secret"] = gitHubSecret
	c.Data["test_secret"] = testSecret	

	c.TplName = "settings.tpl"
}




type DeleteAppController struct {
	beego.Controller
}

func getDeleteOptions() *api.DeleteOptions {
	r := new(api.DeleteOptions)
	return r
}

func (c *DeleteAppController) Get() {
	fmt.Printf("DeleteAppController Get\n")
	
	appname := c.GetString("appname")
	kubernetesClient := getKubernetesClient()
	

//	selector,_ := labels.Parse("app=" + appname) 
//	
//	listOptions := api.ListOptions {
//		LabelSelector: selector,
//	}
//	
//	pods, err := kubernetesClient.Core().Pods(ENV_KUBERNETES_NAMESPACE).List(listOptions)
//	if err != nil {
//		panic(err.Error())
//	}
//	
//	for _,pod := range pods.Items {
//	  kubernetesClient.Pods(ENV_KUBERNETES_NAMESPACE).Delete(pod.Name, getDeleteOptions())
//	}
		
	kubernetesClient.Deployments(ENV_KUBERNETES_NAMESPACE).Delete(appname,getDeleteOptions())
	kubernetesClient.Services(ENV_KUBERNETES_NAMESPACE).Delete(appname,getDeleteOptions())
	
	c.Redirect("/applications", 302)
}




type ViewApplicationsController struct {
	beego.Controller
}


func (c *ViewApplicationsController) Get() {	
	fmt.Printf("ViewApplicationsController \n")
	
	
	kubernetesClient := getKubernetesClient()
	
	services, err := kubernetesClient.Core().Services(ENV_KUBERNETES_NAMESPACE).List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	}   
	
	c.Data["namespace"] = ENV_KUBERNETES_NAMESPACE

	c.Data["services"] = services
	c.Data["servicelist"] = services.Items
	
	c.TplName = "view_applications.tpl"
}



type ViewClusterController struct {
	beego.Controller
}


func (c *ViewClusterController) Get() {	
	fmt.Printf("ViewClusterController \n")
	
	listOptions := api.ListOptions {
	}
	
	kubernetesClient := getKubernetesClient()
	
	pods, err := kubernetesClient.Core().Pods(ENV_KUBERNETES_NAMESPACE).List(listOptions)
	if err != nil {
		panic(err.Error())
	}

	services, err := kubernetesClient.Core().Services(ENV_KUBERNETES_NAMESPACE).List(listOptions)
	if err != nil {
		panic(err.Error())
	} 
	
	c.Data["namespace"] = ENV_KUBERNETES_NAMESPACE

	c.Data["services"] = services
	c.Data["servicelist"] = services.Items
	
	c.Data["pods"] = pods
	c.Data["podlist"] = pods.Items

	c.TplName = "view_cluster.tpl"
}


type DockerRegisteryController struct {
	beego.Controller
}

func (c *DockerRegisteryController) Get() {
	
	kubernetesClient := getKubernetesClient()
	
	secret,err1 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("docker-registery-secret")
	if err1 != nil {
		panic(err1.Error())
	}
	
	hub, err := registry.New(string(secret.Data["url"]), string(secret.Data["username"]), string(secret.Data["password"]))

	repositories, err := hub.Repositories()
	if err != nil {
		c.Redirect("/settings", 302)	
	}

	c.Data["repositories"] = repositories
		
	c.TplName = "docker_registry.tpl"
}

type DockerRepositoryController struct {
	beego.Controller
}

func (c *DockerRepositoryController) Get() {
	
	repository := c.GetString("repository")
	
	kubernetesClient := getKubernetesClient()
	
	secret,err1 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("docker-registery-secret")
	if err1 != nil {
		panic(err1.Error())
	}
	
	hub, err := registry.New(string(secret.Data["url"]), string(secret.Data["username"]), string(secret.Data["password"]))

	tags, err := hub.Tags(repository)
	if err != nil {
		panic(err.Error())
	}

	c.Data["Response"] = tags
		
	c.TplName = "docker_repository.tpl"
}



type GithubRepoAppController struct {
	beego.Controller
}

func (c *GithubRepoAppController) Get() {
	
	kubernetesClient := getKubernetesClient()
	
	secret,err1 :=kubernetesClient.Core().Secrets(ENV_KUBERNETES_NAMESPACE).Get("github-secret")
	if err1 != nil {
		panic(err1.Error())
	}
			
	ts := oauth2.StaticTokenSource(
	    &oauth2.Token{AccessToken: string(secret.Data["token"]) },
	)
	
	tc := oauth2.NewClient(oauth2.NoContext, ts)
	
	//corpDefaultBaseURL := "https://github.corp.ebay.com/"
	corpBaseURL, _ := url.Parse(string(secret.Data["url"]))
		
	client := github.NewClient(tc)
	client.BaseURL = corpBaseURL

	repositories, _, err := client.Repositories.List("", nil)
	if err != nil {
		panic(err.Error())
	}

	c.Data["repositories"] = repositories
	c.Data["Response"] = repositories
		
	c.TplName = "get_github_repos.tpl"
}


