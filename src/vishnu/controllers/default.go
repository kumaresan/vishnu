package controllers

import (
	"github.com/astaxie/beego"
	"flag"
	"fmt"
	"k8s.io/client-go/1.4/kubernetes"
	"k8s.io/client-go/1.4/pkg/api"
	"k8s.io/client-go/1.4/tools/clientcmd"	
)


var (
	kubeconfig = flag.String("kubeconfig", "conf/kubeconfig.conf", "absolute path to the kubeconfig file")
	globalconfig,err1 = clientcmd.BuildConfigFromFlags("", *kubeconfig)
	globalclientset,err2 = kubernetes.NewForConfig(globalconfig)
)

func init() {
	fmt.Printf("kubeconfig            : %s\n", *kubeconfig)
}
type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.TplName = "index.tpl"
}


//    beego.Router("/get/componentstatuses", &controllers.ComponentstatusesController{})


type ComponentstatusesController struct {
	beego.Controller
}
func (c *ComponentstatusesController) Get() {
	fmt.Printf("ComponentstatusesController Get\n")
	componentstatuses, err := globalclientset.Core().ComponentStatuses().List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "componentstatuses"
	c.Data["Response"] = componentstatuses
	c.TplName = "get.tpl"
}

//    beego.Router("/get/configmaps", &controllers.ConfigmapsController{})

type ConfigmapsController struct {
	beego.Controller
}
func (c *ConfigmapsController) Get() {
	fmt.Printf("ConfigmapsController Get\n")
	configmaps, err := globalclientset.Core().ConfigMaps("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "configmaps"
	c.Data["Response"] = configmaps
	c.TplName = "get.tpl"
}


//    beego.Router("/get/daemonsets", &controllers.DaemonsetsController{})
type DaemonsetsController struct {
	beego.Controller
}
func (c *DaemonsetsController) Get() {
	fmt.Printf("DaemonsetsController Get\n")
	daemonsets, err := globalclientset.DaemonSets("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "daemonsets"
	c.Data["Response"] = daemonsets
	c.TplName = "get.tpl"
}


//    beego.Router("/get/deployments", &controllers.DeploymentsController{})
type DeploymentsController struct {
	beego.Controller
}
func (c *DeploymentsController) Get() {
	fmt.Printf("DeploymentsController Get\n")
	deployments, err := globalclientset.Deployments("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "deployments"
	c.Data["Response"] = deployments
	c.TplName = "get.tpl"
}

//    beego.Router("/get/events", &controllers.EventsController{})
type EventsController struct {
	beego.Controller
}
func (c *EventsController) Get() {
	fmt.Printf("EventsController Get\n")
	events, err := globalclientset.Core().Events("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "events"
	c.Data["Response"] = events
	c.TplName = "get.tpl"
}

//    beego.Router("/get/endpoints", &controllers.CndpointsController{})
type CndpointsController struct {
	beego.Controller
}
func (c *CndpointsController) Get() {
	fmt.Printf("CndpointsController Get\n")
	endpoints, err := globalclientset.Core().Endpoints("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "endpoints"
	c.Data["Response"] = endpoints
	c.TplName = "get.tpl"
}

//    beego.Router("/get/horizontalpodautoscalers", &controllers.HorizontalpodautoscalersController{})
type HorizontalpodautoscalersController struct {
	beego.Controller
}
func (c *HorizontalpodautoscalersController) Get() {
	fmt.Printf("HorizontalpodautoscalersController Get\n")
	horizontalpodautoscalers, err := globalclientset.HorizontalPodAutoscalers("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "horizontalpodautoscalers"
	c.Data["Response"] = horizontalpodautoscalers
	c.TplName = "get.tpl"
}

//    beego.Router("/get/ingress", &controllers.IngressController{})
type IngressController struct {
	beego.Controller
}
func (c *IngressController) Get() {
	fmt.Printf("IngressController Get\n")
	ingress, err := globalclientset.Ingresses("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "ingress"
	c.Data["Response"] = ingress
	c.TplName = "get.tpl"
}

//    beego.Router("/get/jobs", &controllers.JobsController{})
type JobsController struct {
	beego.Controller
}
func (c *JobsController) Get() {
	fmt.Printf("JobsController Get\n")
	//jobs, err := globalclientset.Jobs("kman").List(api.ListOptions{})
	//if err != nil {
	//	panic(err.Error())
	//} 
	c.Data["Request"] = "jobs"
	c.Data["Response"] = "TBD"
	c.TplName = "get.tpl"
}

//    beego.Router("/get/limitranges", &controllers.LimitrangesController{})
type LimitrangesController struct {
	beego.Controller
}
func (c *LimitrangesController) Get() {
	fmt.Printf("LimitrangesController Get\n")
	limitranges, err := globalclientset.Core().LimitRanges("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "limitranges"
	c.Data["Response"] = limitranges
	c.TplName = "get.tpl"
}

//    beego.Router("/get/nodes", &controllers.NodesController{})
type NodesController struct {
	beego.Controller
}
func (c *NodesController) Get() {
	fmt.Printf("NodesController Get\n")
	nodes, err := globalclientset.Core().Nodes().List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "nodes"
	c.Data["Response"] = nodes
	c.TplName = "get.tpl"
}


//    beego.Router("/get/namespaces", &controllers.NamespacesController{})
type NamespacesController struct {
	beego.Controller
}
func (c *NamespacesController) Get() {
	fmt.Printf("NamespacesController Get\n")
	namespaces, err := globalclientset.Core().Namespaces().List(api.ListOptions{}) 
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "namespaces"
	c.Data["Response"] = namespaces
	c.TplName = "get.tpl"
}


//    beego.Router("/get/pods", &controllers.PodsController{})

type PodsController struct {
	beego.Controller
}

func (c *PodsController) Get() {
	fmt.Printf("podsController Get\n")

	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)

	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	
	pods, err := clientset.Core().Pods("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	}
	c.Data["podlist"] = pods.Items
	
	c.Data["Request"] = "pods"
	c.Data["Response"] = pods
	c.TplName = "pods.tpl"
}



//    beego.Router("/get/persistentvolumes", &controllers.PersistentvolumesController{})
type PersistentvolumesController struct {
	beego.Controller
}
func (c *PersistentvolumesController) Get() {
	fmt.Printf("PersistentvolumesController Get\n")
	persistentvolumes, err := globalclientset.Core().PersistentVolumes().List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "persistentvolumes"
	c.Data["Response"] = persistentvolumes
	c.TplName = "get.tpl"
}

//    beego.Router("/get/persistentvolumeclaims", &controllers.PersistentvolumeclaimsController{})
type PersistentvolumeclaimsController struct {
	beego.Controller
}
func (c *PersistentvolumeclaimsController) Get() {
	fmt.Printf("PersistentvolumeclaimsController Get\n")
	persistentvolumeclaims, err := globalclientset.Core().PersistentVolumeClaims("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "persistentvolumeclaims"
	c.Data["Response"] = persistentvolumeclaims
	c.TplName = "get.tpl"
}

//    beego.Router("/get/quota", &controllers.QuotaController{})
type QuotaController struct {
	beego.Controller
}
func (c *QuotaController) Get() {
	fmt.Printf("QuotaController Get\n")
	//quota, err := globalclientset.Core().ConfigMaps("kman").List(api.ListOptions{})
	//if err != nil {
	//	panic(err.Error())
	//} 
	c.Data["Request"] = "quota"
	c.Data["Response"] = "TBD"
	c.TplName = "get.tpl"
}

//    beego.Router("/get/resourcequotas", &controllers.ResourcequotasController{})
type ResourcequotasController struct {
	beego.Controller
}
func (c *ResourcequotasController) Get() {
	fmt.Printf("ResourcequotasController Get\n")
	resourcequotas, err := globalclientset.Core().ResourceQuotas("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "resourcequotas"
	c.Data["Response"] = resourcequotas
	c.TplName = "get.tpl"
}

//    beego.Router("/get/replicasets", &controllers.ReplicasetsController{})
type ReplicasetsController struct {
	beego.Controller
}
func (c *ReplicasetsController) Get() {
	fmt.Printf("ReplicasetsController Get\n")
	replicasets, err := globalclientset.ReplicaSets("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "replicasets"
	c.Data["Response"] = replicasets
	c.TplName = "get.tpl"
}

//    beego.Router("/get/replicationcontrollers", &controllers.ReplicationcontrollersController{})
type ReplicationcontrollersController struct {
	beego.Controller
}
func (c *ReplicationcontrollersController) Get() {
	fmt.Printf("ReplicationcontrollersController Get\n")
	replicationcontrollers, err := globalclientset.Core().ReplicationControllers("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "replicationcontrollers"
	c.Data["Response"] = replicationcontrollers
	c.TplName = "get.tpl"
}

//    beego.Router("/get/secrets", &controllers.SecretsController{})
type SecretsController struct {
	beego.Controller
}
func (c *SecretsController) Get() {
	fmt.Printf("SecretsController Get\n")
	secrets, err := globalclientset.Core().Secrets("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "secrets"
	c.Data["Response"] = secrets
	c.TplName = "get.tpl"
}

//    beego.Router("/get/serviceaccounts", &controllers.ServiceaccountsController{})
type ServiceaccountsController struct {
	beego.Controller
}
func (c *ServiceaccountsController) Get() {
	fmt.Printf("ServiceaccountsController Get\n")
	serviceaccounts, err := globalclientset.Core().ServiceAccounts("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "serviceaccounts"
	c.Data["Response"] = serviceaccounts
	c.TplName = "get.tpl"
}

//    beego.Router("/get/services", &controllers.ServicesController{})
type ServicesController struct {
	beego.Controller
}
func (c *ServicesController) Get() {
	fmt.Printf("ServicesController Get\n")
	services, err := globalclientset.Core().Services("kman").List(api.ListOptions{})
	if err != nil {
		panic(err.Error())
	} 
	c.Data["Request"] = "services"
	c.Data["Response"] = services
	c.TplName = "get.tpl"
}

