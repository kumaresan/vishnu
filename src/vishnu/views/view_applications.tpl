{{ template "layout_app.tpl" . }}


{{ define "content" }}
               
        <h1 class="page-header"> Applications </h1>
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Services Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>                
			 	{{range $key, $val := .servicelist}}
			 	<tr>
			 		<td>{{$key}}</td>
			 		<td>{{$val.Name}}</td>
			 		<td><a href="/viewapp?appname={{$val.Name}}"> View </a></td>
			    </tr>
			    {{end}}               
                     
              </tbody>
            </table>
        </div>
        

{{ end }}

