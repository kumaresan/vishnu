<!DOCTYPE html>
<html>
<head>
    <title>Vishnu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/static/css/dashboard.css" >
</head>
<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Vishnu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">          
            <li><a href="/get/componentstatuses">Component Statuses</a></li>
            <li><a href="/get/configmaps">Config Maps</a></li>
            <li><a href="/get/daemonsets">Daemon Sets</a></li>
            <li><a href="/get/deployments">Deployments</a></li>
            <li><a href="/get/events">Events</a></li>
            <li><a href="/get/endpoints">Endpoints</a></li>
            <li><a href="/get/horizontalpodautoscalers">Horizontal Pod Autoscalers</a></li>
            <li><a href="/get/ingress">Ingress</a></li>
            <li><a href="/get/jobs">Jobs</a></li>
            <li><a href="/get/limitranges">Limit Ranges</a></li>
            <li><a href="/get/nodes">Nodes</a></li>
            <li><a href="/get/namespaces">Namespaces</a></li>
            <li><a href="/get/pods">Pods</a></li>
            <li><a href="/get/persistentvolumes">Persistent Volumes</a></li>
            <li><a href="/get/persistentvolumeclaims">Persistent Volume Claims</a></li>
            <li><a href="/get/quota">Quota</a></li>
            <li><a href="/get/resourcequotas">Resource Quotas</a></li>
            <li><a href="/get/replicasets">Replicasets</a></li>
            <li><a href="/get/replicationcontrollers">Replication Controllers</a></li>
            <li><a href="/get/secrets">secrets</a></li>         
            <li><a href="/get/serviceaccounts">Service Accounts</a></li>         
            <li><a href="/get/services">Services</a></li>         
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    
          {{template "content" .}}
        
        </div>
      </div>
    </div>



    <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

</body>
</html>