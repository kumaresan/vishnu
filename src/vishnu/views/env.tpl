{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Environment</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

           <div class="row">
                <div class="col-lg-12">
		        
		    		<div class="table-responsive">
			            <table class="table table-striped">
			              <thead>
			                <tr>
			                  <th>Environment Variable</th>
			                  <th>Value</th>
			                </tr>
			              </thead>
			              <tbody>               		              
							<tr><td>KUBERNETES_IN_CLUSTER</td><td>{{ .KUBERNETES_IN_CLUSTER}}</td></tr>
							<tr><td>KUBERNETES_URL</td><td>{{ .KUBERNETES_URL}}</td></tr>
							<tr><td>KUBERNETES_NAMESPACE</td><td>{{ .KUBERNETES_NAMESPACE}}</td></tr>
							<tr><td>KUBERNETES_BEARER_TOKEN</td><td>{{ .KUBERNETES_BEARER_TOKEN}}</td></tr>
							<tr><td>DOCKER_REGISTRY_URL</td><td>{{ .DOCKER_REGISTRY_URL}}</td></tr>
							<tr><td>DOCKER_REGISTRY_USERNAME</td><td>{{ .DOCKER_REGISTRY_USERNAME}}</td></tr>
							<tr><td>DOCKER_REGISTRY_PASSWORD</td><td>{{ .DOCKER_REGISTRY_PASSWORD}}</td></tr>
							<tr><td>GITHUB_URL</td><td>{{ .GITHUB_URL}}</td></tr>
							<tr><td>GITHUB_ACCESS_TOKEN</td><td>{{ .GITHUB_ACCESS_TOKEN}}</td></tr>			              
							<tr><td>Response</td><td>{{ .Response}}</td></tr>
			              </tbody>
			            </table>
			        </div>		        
                </div>
            </div>
{{ end }}

