{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Settings</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Docker Registery Settings
                        </div>
                        <div class="panel-body">
                            <form role="form" method="get" action="/updatedrsettings">
                                <div class="form-group">
                                    <label>URL</label>
                                    <input class="form-control" name="drurl" value="{{ .docker_registery_url }}">
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" name="drusername" value="{{ .docker_registery_username }}">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" name="drpassword" value="{{ .docker_registery_password }}">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
							</form>
                        </div>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Github Settings
                        </div>
                        <div class="panel-body">
                            <form role="form" method="get" action="/updateghsettings">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" name="ghurl" value="{{ .github_url }}">
                                </div>
                                <div class="form-group">
                                    <label>Token</label>
                                    <input class="form-control" name="ghtoken" value="{{ .github_token }}">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
							</form>
                        </div>
                    </div>
                </div>

            </div>
            
        	<div class="row">
        	
        		<h1 class="page-header"> Docker Registery Secret </h1>
        		<div class="well"> <p> {{ .docker_registery_secret }} </p> </div>
        

        		<h1 class="page-header"> Github Secret </h1>
        		<div class="well"> <p> {{ .github_secret }} </p> </div>        	


        		<h1 class="page-header"> Test Secret </h1>
        		<div class="well"> <p> {{ .test_secret }} </p> </div>        	
        		<div class="well"> 
        			<p> {{ .test_username }} </p>
        			<p> {{ .test_password }} </p> 
        			<p> {{ .raw_test_username }} </p>
        			<p> {{ .raw_test_password }} </p> 
    			</div>
        	
        	</div>
        
        
{{ end }}

