{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create Node App</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" action="/action/createnodeapp" method="GET">
                        <div class="form-group">
                            <label>App Name</label>
                            <input name="appname" class="form-control">
                            <p class="help-block">Provide a unique app name here.</p>
                        </div>
                    </form>
                 </div>
            </div>
{{ end }}

