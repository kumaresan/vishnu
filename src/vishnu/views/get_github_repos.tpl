{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
			        <h1 class="page-header"> Github Repo</h1>
			        <div class="table-responsive">
			            <table class="table table-striped">
			              <thead>
			                <tr>
			                  <th>#</th>
			                  <th>Repo Name</th>
			                  <th>Action</th>
			                </tr>
			              </thead>
			              <tbody>                
						 	{{range $key, $val := .repositories}}
						 	<tr>
						 		<td>{{$key}}</td>
						 		<td>{{$val.Name}}</td>
						 		<td><a href="/blank">Deploy (TBD)</a></td>
						    </tr>
						    {{end}}               
			                     
			              </tbody>
			            </table>
			        </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>




           <!--div class="row">
                <div class="col-lg-12">
		        <div class="well"> <p> {{ .Response }} </p> </div>
                </div>
            </div-->
{{ end }}

