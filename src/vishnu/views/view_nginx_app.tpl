{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Nginx App</h1>
                </div>
                
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
			        <div class="well"> 
			        	<p> 			     
			        		<a href="/create/nginxapp" class="btn btn-primary btn-lg">Create</a> 
			        		<a href="/delete/nginxapp" class="btn btn-danger btn-lg">Delete</a>
			        	</p> 
			        </div>
               	</div>
            </div>


           <div class="row">
                <div class="col-lg-12">
                
                	<div class="table-responsive">
			            <table class="table table-striped">
			              <thead>
			                <tr>
			                  <th>#</th>
			                  <th>Pod Name</th>
			                </tr>
			              </thead>
			              <tbody>                
						 	{{range $key, $val := .podlist}}
						 	<tr>
						 		<td>{{$key}}</td>
						 		<td>{{$val.Name}}</td>
						    </tr>
						    {{end}}               
			                     
			              </tbody>
			            </table>
	        		</div>
                
               
		        	<div class="well"> <p> {{ .Response }} </p> </div>
                </div>
            </div>
{{ end }}

