{{ template "layout_app.tpl" . }}


{{ define "content" }}
           <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Docker Registery</h1>

			       <div class="row">
			            <div class="col-lg-12">
			            	<form action="/dockerrepository" method="GET">
						        <div class="form-group input-group">
		                            <input type="text" name="repository" class="form-control">
		                            <span class="input-group-btn">
		                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		                            </span>
		                        </div>
                            </form>
			            </div>
			        </div>
 
			         <div class="table-responsive">
			            <table class="table table-striped">
			              <thead>
			                <tr>
			                  <th>#</th>
			                  <th>Repository Name</th>
			                  <th>Action</th>
			                </tr>
			              </thead>
			              <tbody>                
						 	{{range $key, $val := .repositories}}
						 	<tr>
						 		<td>{{$key}}</td>
						 		<td>{{$val}}</td>
						 		<!-- td><a href="/dockerrepository?repository={{$val}}">View</a -->
						 		<td><a href="/createappform?apptype=generic&dockerimage=ecr.vip.ebayc3.com/{{$val}}">Deploy</a>
						    </tr>
						    {{end}}               			                     			                     
			                     
			              </tbody>
			            </table>
			        </div>			 
 
                </div>
                <!-- /.col-lg-12 -->
            </div>


{{ end }}

