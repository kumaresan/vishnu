# Vishnu

Vishnu is a simple webapp for managing a kubernetes cluster. Its written in Go using Beego web framework.

### Env Variables
* KUBERNETES_URL
* KUBERNETES_NAMESPACE
* KUBERNETES_BEARER_TOKEN

* DOCKER_REGISTRY_URL
* DOCKER_REGISTRY_USERNAME
* DOCKER_REGISTRY_PASSWORD

* GITHUB_URL 
* GITHUB_ACCESS_TOKEN

### Versions


0.6 (TBD)
 - TBD : Git integration.
 - TBD : Git Auto deploy 
 - TBD : Git to Deploy Pipeline   

0.5 (TBD)
 - TBD : Create and View Raptor APP
 - TBD : Successfully Delete App and Pods 
 - TBD : More options while creating an App. 
 - TBD : Redeploy App
 - TBD : Scale App (add new replicas)
 - TBD : Cluster : Add more relevant information (Deployment, Nodes, etc)
 - TBD : Docker Registery : show tags 
 - TBD : Move env variable to Kubernetes Secrets. 
 - TBD : Kubernetes integrations with Keystone. (oauth)
 - TBD : Kubernetes deployment in Kubernetes.   

0.4
  - Code Cleanup create a new controller file app_controller.go
  - View Cluster
  - View Docker Registery and deploy from registery
  - View Git Repo (TBD : Deploy from Git)
  - Create a brand new nginx app. 
  - Create a deployment by the same name in kubernetes
  - Create a service by the same name in kubernetes. 
  - View the App Link on the View Item page
  - Delete the service in kubernetes
  - Delete the deployment in kubernetes.
  - Deploy a Raptor App and view VI (TBD : Validate the app)    

0.3
  - Client for Docker Registery
  - Client for Github
  - Externalized all the Secrets  

0.2
  - Basic Nginx app creation / deletion completed.  

0.1
  - Basic webapp setup
  

### TODO
  - Get name of the app, List of app nginx
  - deploy node app
  - deploy a mongo db app, create a volume etc. 
  - deploy a mean stack (node, express, angular & mongo db)
  - Pull images from docker registory. 
  - create git repo for created app. 
  - attach git hooks to repo and trigger redeploy on git commit.
  - Jenkins and ci setup.
  - Autoscale applications 
  - Team management via Git
  - Dynamic Environment deployment. 
  - Complete deployment management via Git. 
  - Control Plane and Non-Control Plain deployment
  - Config management
  - Secrets Management.
  - Log Management
  - Monitoring 
  - Add Tests. 


### Setup Commands
```sh
$ set GOPATH=C:\Users\kmanickavelu\workspace\neongo\vishnu
$ go get github.com/astaxie/beego
$ go get github.com/beego/bee
$ go get k8s.io/client-go/1.4/kubernetes
$ go get github.com/heroku/docker-registry-client
$ go get github.com/docker/distribution
$ go get github.com/docker/distribution
$ go get github.com/google/go-github
$ go get github.com/google/go-querystring
```



Contact @[Kumaresan]

   [kubeconfig]: <http://kubernetes.io/docs/user-guide/kubeconfig-file/>
   [Kumaresan]: <https://twitter.com/kumaresan>

